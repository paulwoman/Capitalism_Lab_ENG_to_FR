[HEADER]
Title=Profit with Purpose 
Description=Your mission is to build a thriving company that balances financial success with social responsibility. Aim for $500 million in annual revenue while employing 1,000 workers.^^Invest in your workforce with training and have 90% of functional units reaching level 7. Strive for excellence by ensuring 90% of products achieve quality ratings of 70.^^Show your commitment to the community by donating 10% of lifetime profits through the Community Engagement Department in your corporate Headquarters.^^Can you achieve sustainable growth and positive impact?
Difficulty Rating=130
DLC=CityEcoSim 
Survival Mode=No
Reset All Settings=Yes

[ENVIRONMENT]
Number of Cities=3
Your Start-up Capital=Very High 
Random Events=Seldom 
Game Starting Year=2000
Retail Store Type=Many
Technology Disruption=On
Stock Market=Yes
Alternative Stock Sim=No
Boom-Bust Cycle Volatility=Low
Macro Economy Realism=High
Inflation=On
Inflation Strength=Normal

[COMPETITORS]
Number of Competitors=4
Competitor Start-up Capital=Moderate
AI Expansion Aggressiveness=Moderate
AI Pricing Aggressiveness=Low
AI Expertise Level=Low
AI Tech Head Start=Low
Show Competitor Trade Secrets=Yes
AI Friendly Merger=Off
Competence of Local Competitors=Low

[PRODUCTS]
Consumer Goods Seaports=3
Industrial Goods Seaports=3
Constant Import Supply=Yes
Import Quality=High

[SPECIAL RULES]
COO Salary Modifier=50
CTO Salary Modifier=50
CMO Salary Modifier=50


[MAIN GOAL]
Goal Title=Profit with Purpose 
Goal Description=This scenario challenges you to balance financial success with social responsibility in 10 years. Grow your company to $500M revenue with 1000 employees while maintaining 15% profit margin and return on equity. Keep CEO pay in check, invest in training, and aim for high product quality, while donating 10% of lifetime profits to civic causes.
Goal Achieved Text=Congratulations! You've masterfully balanced profit and purpose, proving that ethical business practices and financial success can go hand in hand – your company stands as a beacon of responsible capitalism and a model for the future of business.
Number of Game Years=11

[MAIN GOAL VALUES]
Player Company Annual Revenue=500
Total number of Employees=1000
Operating Profit Margin=15
Return on Equity=15
CEO-to-Employee Pay Ratio=30
Target functional unit level=7,90
Target Product quality rating=70,90
Civic Donation as % of Lifetime Profit=10
Number of Game Years Played=10

[MAIN GOAL REWARDS] 
Score change=1000

//-------------------------------------------------------------------//

[CHALLENGE GAME]
Number of Game Years=11
Ranking Method=Goal+Score
