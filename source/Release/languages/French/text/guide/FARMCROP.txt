[Crop Growing Unit]

Crop growth units at farms cultivate and harvest crops. When you tell a crop growing unit which crop to grow, it takes care of the growing and harvesting processes and keeps the harvested crops in its barn.^

~Crop Name~
The type of crop that being grown.^ 
~Crop in Barn~
When crops are harvested, they are stored in barns, which are represented by the blue bars. The blue bars increases in length when crops are being harvested. Conversely, their lengths decrease when crops are being sold.^
~Growth Progress~
The bar indicates the growth progress of the crop.
